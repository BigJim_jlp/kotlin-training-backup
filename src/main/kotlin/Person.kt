fun main() {
    multiPerson()
//    val james = Person("James", "Holton", 200.5, "Brown", false)
//    val firstName = james.firstName
//    val height = james.height
//    val hair = james.hair
//
//    if (james.height > 200) {
//        println("$firstName, is very tall at $height cms")
//    }
//
//    if (james.glasses) {
//        println("Hi $firstName, your hair is $hair")
//    } else {
//        println("You don't wear Glasses")
//    }
}

class Person(
        val firstName: String,
        val lastName: String,
        val height: Double,
        val hair: String,
        val glasses: Boolean)

fun multiPerson() {
    val james = Person("James", "Holton", 200.0, "Brown", false)
    val lena = Person("Lena", "Dack", 150.0, "Blonde", true)
    val lottie = Person("Lottie", "Holton", 100.0, "Blonde", false)
    val francesca = Person("Francesca", "Holton", 90.0, "Red", false)
    val family = listOf(james, lena, lottie, francesca)

    for (person in family) {
        if (person.glasses) {
            println(person.firstName + " " + person.lastName)
        }
    }
    println(family)
}

