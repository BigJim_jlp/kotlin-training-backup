fun main() {
    var dog1 = Dog("Bob", "Poodle", 2)
    var dog2 = Dog("Fred", "Great Dane", 3)
    var dog3 = Dog("Fido", "Dachshund", 10)
    var dog4 = Dog("Rover", "Poodle", 5)
    var dog5 = Dog("Gloria", "Jack Russel", 6)
    var dog6 = Dog("Fleur", "Great Dane", 1)
    var dog7 = Dog("Sparkle", "Poodle", 7)
    var dog8 = Dog("Dog", "Mongrel", 15)

    var dogList = listOf(dog1, dog2, dog3, dog4, dog5, dog6, dog7, dog8)

    println("What would you like to do?")
    println("1 - Search by Breed")
    println("2 - Search by Age")
    var selection = readLine();

    if (selection != null) {
        if (selection.toInt() == 1) {
            breedSearch(dogList)
        } else {
            ageSearch(dogList)
        }
    }


}

fun breedSearch(dogList: List<Dog>) {
    println("What Breed would you like to see?")
    var inputBreed = readLine();

    for (dog in dogList) {
        if (inputBreed != null) {
            if (dog.breed.toUpperCase() == inputBreed.toUpperCase()) {
                var name = dog.name
                println("Dog called $name is a ${inputBreed.toLowerCase().capitalize()}")
            }
        }
    }
}

fun ageSearch(dogList: List<Dog>) {
    println("What age of dog would you like to see?")
    var inputAge = readLine();

    for (dog in dogList) {
        if (inputAge != null) {
            if (dog.age == inputAge.toInt()) {
                var name = dog.name
                println("Dog called $name is $inputAge year(s) old")
            }
        }
    }
}

data class Dog(
        var name: String,
        var breed: String,
        var age: Int
)